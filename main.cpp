#include "patyczak.h"
#include "kinect_data.h"
#include <QApplication>
#include <QMessageBox>
#include <iostream>


using namespace std;


/*!
 * \brief main
 *
 * Plik odpowiedzialny za inicjalizację pracy.
 * \return
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Patyczak okno;
    okno.show();    /**< Pierwszy kontakt z użytkownikiem - wyświetlamy okno główne.*/

    /*!
     * Nawiązanie połączenia z bazą danych.
     * UWAGA
     * Nazwa i ścieżka do bazy są ustawione na stałe.
     */
        //QSqlDatabase kinect_ = QSqlDatabase::addDatabase("QODBC");
        kinect_data() = QSqlDatabase::addDatabase((QString)"QSQLITE");
//        kinect_data().setHostName("localhost");
//        kinect_data().setDatabaseName("kinect_dane");
//        kinect_data().setUserName("patyczak");
//        kinect_data().setPassword("program");
        kinect_data().setDatabaseName("D:/Patyczak/dane_lite.sl3");
        bool ok = kinect_data().open();

     if (!ok)
     {
         QMessageBox msgBox;
         msgBox.setText("Błąd połączenia z bazą danych.");
         msgBox.exec();
     }


    return a.exec();
}
