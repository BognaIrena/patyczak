#include "staw.h"

staw::staw(QString name)
{
    this->nazwa=name;
    this->powodzenie=0;
}

staw::staw()
{
    this->powodzenie=0;
}

staw &staw::odczyt(QTime czas)
{
    QSqlQuery odczyt;
       QString zpt ("SELECT `X`, `Y` FROM `joints_movement` WHERE `joint` = '%1' AND `ID`=%3");
       odczyt.exec(zpt.arg(this->nazwa).arg(badanieID()));

       if (odczyt.next())
       {
           this->position.setX(odczyt.value(0).toFloat()*50);
           this->position.setY(odczyt.value(1).toFloat()*50);
           this->powodzenie=1;
       }
       else
       {
           this->powodzenie=0;
       }

       return *this;
}

staw &staw::odczyt(int point)
{
    QSqlQuery odczyt;
       QString zpt ("SELECT `X`, `Y` FROM `joints_movement` WHERE `joint` = '%1' AND `position_ID` = %2 AND `badanie_ID`=%3");
       odczyt.exec(zpt.arg(this->nazwa).arg(point).arg(badanieID()));

       if (odczyt.next())
       {
           this->position.setX(odczyt.value(0).toFloat());
           this->position.setY(odczyt.value(1).toFloat());
   //        qDebug() << this->nazwa <<"X = "<< odczyt.value(0).toFloat() <<"Y = " << odczyt.value(1).toFloat() << endl;
           qDebug() << this->nazwa <<"X = "<< this->position.x() <<"Y = " << this->position.y() << endl;
           this->powodzenie=1;
       }
       else
       {
           this->powodzenie=0;
       }

       return *this;
}
