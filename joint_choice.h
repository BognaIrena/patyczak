#ifndef JOINT_CHOICE_H
#define JOINT_CHOICE_H

#include <QDialog>
#include "kinect_data.h"
#include "ui_joint_choice.h"

namespace Ui {
class joint_choice;
}

/*!
 * \brief Klasa wyboru stawu.
 *
 * W przypadku śledzenia stawu, ta klasa i powiązane z nią okno dialogowe służą do wyboru dowolnego stawu.
 */
class joint_choice : public QDialog
{
    Q_OBJECT

public:
    explicit joint_choice(QWidget *parent = 0);
    ~joint_choice();

private slots:
    void on_joints_list_itemClicked(QListWidgetItem *item); /**< Wybór i zapisanie żądanego stawu. */

private:
    Ui::joint_choice *ui;
};

#endif // JOINT_CHOICE_H
