#include "kinect_data.h"

QSqlDatabase &kinect_data()
{
    static QSqlDatabase db;
    return db;
}

QDate &dzien()
{
    static QDate X;
    return X;
}

QTime &czas ()
{
    static QTime X;
    return X;
}


int &badanieID()
{
    static int ID;
    return ID;
}

int &tryb()
{
    static int tryb;
    return tryb;
}

QString &pacjent()
{
    static QString pacjent;
    return pacjent;
}


QString &tracked_joint()
{
    static QString tracked_joint;
    return tracked_joint;
}

QString WyswietlDate( const QDate &Date)
{
 QString DateStr("%1-%2-%3");

 return DateStr.arg(Date.year()).arg(static_cast<int>(Date.month()),2,10,QChar('0')).arg(Date.day());
}


int &point()
{
    static int point;
    return point;
}
