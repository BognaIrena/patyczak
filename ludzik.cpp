#include "ludzik.h"

ludzik::ludzik(QWidget *parent) :
    QWidget(parent)
{
    tryb=-1;
    QlaQlho = new QTimer(this);
    QlaQlho->setInterval(500);
    connect(QlaQlho, SIGNAL(timeout()), this, SLOT(repaint()));
}

    void  ludzik::Start()
    {
        active=1;
  //      qDebug() << active;
        connect(QlaQlho, SIGNAL(timeout()), QlaQlho, SLOT(start()));
        QlaQlho->start();
    }

    void ludzik::Stop()
    {
        active=0;
  //      qDebug() << active;
        disconnect(QlaQlho, SIGNAL(timeout()), QlaQlho, SLOT(start()));
        QlaQlho->stop();
    }

    void ludzik::animate()
    {

        qDebug() << "Skeleton animation";

        QColor ArmColor(68, 68, 127);

        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.translate(width() / 2, height() / 2);

        painter.setBrush(ArmColor);

        // Tulow

        QPoint BodyUp[3] =
        {
            Patyczak.stawy[LeftShoulder].position,
            Patyczak.stawy[LeftHip].position,
            Patyczak.stawy[ShoulderCenter].position
        };

        QPoint BodyDown[4] = {Patyczak.stawy[LeftShoulder].position,Patyczak.stawy[LeftHip].position,
                              Patyczak.stawy[RightHip].position, Patyczak.stawy[RightShoulder].position};
        /*
        QPoint Body[6] = {QPoint(-55,-67), QPoint(-7,-104),
                          QPoint(-21,28), QPoint(-24,20),
                          QPoint(0,0), QPoint(50,-80)};
                          */

        // Glowa
        Patyczak.stawy[Head].position.setY(Patyczak.stawy[Head].position.y()+24);
        painter.drawEllipse(Patyczak.stawy[Head].position, 20, 24);
        painter.setBrush(QColor(10,0,0,100));
        painter.drawPolygon(BodyUp, 3);
        painter.setBrush(QColor(10,20,0,100));
        painter.drawPolygon(BodyDown, 4);

        // Lewa reka
        painter.drawLine(Patyczak.stawy[LeftShoulder].position, Patyczak.stawy[LeftElbow].position);
//        qDebug() << "LeftShoulder = " << Patyczak.stawy[LeftShoulder].position << endl;
        painter.drawLine(Patyczak.stawy[LeftElbow].position, Patyczak.stawy[LeftWrist].position);
        painter.drawLine(Patyczak.stawy[LeftWrist].position, Patyczak.stawy[LeftHand].position);

        // Prawa reka
        painter.drawLine(Patyczak.stawy[RightShoulder].position, Patyczak.stawy[RightElbow].position);
        painter.drawLine(Patyczak.stawy[RightElbow].position, Patyczak.stawy[RightWrist].position);
        painter.drawLine(Patyczak.stawy[RightWrist].position, Patyczak.stawy[RightHand].position);

        // Lewa noga
        painter.drawLine(Patyczak.stawy[LeftHip].position, Patyczak.stawy[LeftKnee].position);
        painter.drawLine(Patyczak.stawy[LeftKnee].position, Patyczak.stawy[LeftAnkle].position);
        painter.drawLine(Patyczak.stawy[LeftAnkle].position, Patyczak.stawy[LeftFoot].position);

        //Prawa noga
        painter.drawLine(Patyczak.stawy[RightHip].position, Patyczak.stawy[RightKnee].position);
        painter.drawLine(Patyczak.stawy[RightKnee].position, Patyczak.stawy[RightAnkle].position);
        painter.drawLine(Patyczak.stawy[RightAnkle].position, Patyczak.stawy[RightFoot].position);

        if (active==1)
        {
           Patyczak.refresh();
           Patyczak.czas.addMSecs(25);
           point()+=1;
           emit PointChanged();
        }

    }

    void ludzik::follow()
    {
 //        qDebug() << "Joint animation";

        QColor ArmColor(127, 68, 127);

        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.translate(width() / 2, height() / 2);

        painter.setBrush(ArmColor);

        punkt.nazwa=tracked_joint();
        punkt.odczyt(point());
        trajektoria.push_front(punkt.position);

        for (int i=0; i<trajektoria.size()-1; i++)
        {
            painter.drawLine(trajektoria[i], trajektoria[i+1]);
        }

        if (active==1)
        {
            czas().addMSecs(25);
            point()+=1;
            emit PointChanged();
        }
    }

    void ludzik::paintEvent (QPaintEvent * event)
    {
         if (tryb==1)
         {
 //           qDebug() << "animate";
           this->animate();
         }

         else if (tryb==0)
         {
 //           qDebug() << "follow";
           this->follow();
         }
    }

    szkielet::szkielet()
    {
        stawy [Head].nazwa ="Head";
        stawy [ShoulderCenter].nazwa="ShoulderCenter";
        stawy [Spine].nazwa =        "Spine";
        stawy [HipCenter].nazwa =    "HipCenter";

        stawy [LeftShoulder].nazwa = "LeftShoulder";
        stawy [LeftElbow].nazwa =    "LeftElbow";
        stawy [LeftWrist].nazwa =    "LeftWrist";
        stawy [LeftHand].nazwa =     "LeftHand";

        stawy [LeftHip].nazwa =      "LeftHip";
        stawy [LeftKnee].nazwa =     "LeftKnee";
        stawy [LeftAnkle].nazwa =    "LeftAnkle";
        stawy [LeftFoot].nazwa =     "LeftFoot";

        stawy [RightShoulder].nazwa ="RightShoulder";
        stawy [RightElbow].nazwa =   "RightElbow";
        stawy [RightWrist].nazwa =   "RightWrist";
        stawy [RightHand].nazwa =    "RightHand";

        stawy [RightHip].nazwa =     "RightHip";
        stawy [RightKnee].nazwa =    "RightKnee";
        stawy [RightAnkle].nazwa =   "RightAnkle";
        stawy [RightFoot].nazwa =    "RightFoot";
    }

    void szkielet::refresh()
    {
        for (int staw=HipCenter; staw<=RightFoot; staw++)
        {
           // stawy[staw].odczyt(this->czas);
            stawy[staw].odczyt(point());
        }

    }
