#ifndef LUDZIK_H
#define LUDZIK_H

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include "kinect_data.h"
#include "staw.h"
#include "joint_choice.h"
#include "choice.h"
#include <QTimer>

/*!
 * \brief Lista stawów odczytywanych przez Kinecta.
 */
enum joints
{
     HipCenter,
     Spine,
     ShoulderCenter,
     Head,

     LeftShoulder,
     LeftElbow,
     LeftWrist,
     LeftHand,

     RightShoulder,
     RightElbow,
     RightWrist,
     RightHand,

     LeftHip,
     LeftKnee,
     LeftAnkle,
     LeftFoot,

     RightHip,
     RightKnee,
     RightAnkle,
     RightFoot
};

/*!
 * \brief Struktura szkieletu - wszystkie stawy wraz z położeniem w danej chwili.
 */
struct szkielet
{
    staw stawy [20];
    QTime czas;

    szkielet();
    /*!
     * \brief Odświeżanie całego szkieletu naraz.
     * \param czas
     */
    void refresh();
};

/*!
 * \brief Klasa odpowiedzialna za rysowanie postaci na podstawie punktów zaczerpniętych z bazy danych.
 */
class ludzik : public QWidget
{
    Q_OBJECT

   QTimer* QlaQlho; /**< Timer wykorzystywany do odświeżania obrazu. */
public:

    /*!
     * \brief Widget "ludzik" osadzany w głównym oknie.
     * \param parent
     */
    explicit ludzik(QWidget *parent = 0);
    int tryb;

    /*!
     * \brief Zmienna pomocnicza "active".
     *
     * Służy do zatrzymywania lub przywracania odświeżania obrazu w razie wciśnięcia przycisku "Stop" lub "Start".
     */
    bool active;
    szkielet Patyczak; /**< Szkielet, do którego zapisywane są dane poszczególnych pozycji po odczytaniu z bazy.*/
    staw punkt; /**< Staw przechowujący najświeższe dane do trajektorii.*/
    QList <QPoint> trajektoria; /**< Lista punktów, przez które przeszedł wybrany staw, służąca do rysowania trajektorii.*/



    /*!
     * \brief "animate" rysuje postać.
     *
     * Zestaw instrukcji służący do animowania patyczakowatej postaci.
     */
    void animate();

    /*!
     * \brief "follow" rysuje trajektorię.
     *
     * Zestaw instrukcji służący do wyrysowywania trajektorii wybranego stawu.
     */
    void follow();

    /*!
     * \brief paintEvent do wywoływania metod rodziny Draw.
     *
     * Odpowiedzialny za rysowanie całej postaci lub śledzenie trajketorii pojedynczego stawu,
     * zależnie od wyboru użytkownika
     * \param event
     */
    virtual void paintEvent(QPaintEvent* event);
signals:
    void PointChanged();

public slots:

    /*!
     * \brief Start
     *
     * Ustawia zmienną kontrolną "active" na wartość 1. Aktywowany sygnałem kliknięcia przycisku "Start" w oknie głównym.
     */
    void Start();

    /*!
     * \brief Stop
     *
     * Ustawia zmienną kontrolną "active" na wartość 0. Aktywowany sygnałem kliknięcia przycisku "Stop" w oknie głównym.
     */
    void Stop();
};

#endif // LUDZIK_H
