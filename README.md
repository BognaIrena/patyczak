# README #
Projekt Patyczak został zrealizowany na zaliczenie kursu Wizualizacja Danych Sensorycznych prowadzonego przez dr. Bogdana Kreczmera na kierunku Automatyka i Robotyka, W-4, Politechnika Wrocławska.

Celem projektu było stworzenie interfejsu graficznego pobierającego z bazy danych (tworzonej równolegle jako projekt z kursu Bazy Danych) informacje o kolejnych położeniach poszczególnych przegubów postaci ludzkiej zarejestrowanej czujnikiem Kinect i prezentującego go w formie prostej animacji ludzika (stąd nazwa Patyczak).