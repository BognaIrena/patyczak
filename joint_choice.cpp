#include "joint_choice.h"
#include "ui_joint_choice.h"

joint_choice::joint_choice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::joint_choice)
{
    ui->setupUi(this);
    setWindowTitle(tr("Wybór stawu"));
    QStringList joints;
    joints << "HipCenter"
           << "Spine"
           << "ShoulderCenter"
           << "Head"

           << "LeftShoulder"
           << "LeftElbow"
           << "LeftWrist"
           << "LeftHand"

           << "LeftHip"
           << "LeftKnee"
           << "LeftAnkle"
           << "LeftFoot"

           << "RightShoulder"
           << "RightElbow"
           << "RightWrist"
           << "RightHand"

           << "RightHip"
           << "RightKnee"
           << "RightAnkle"
           << "RightFoot";

    ui->joints_list->addItems(joints);
}

joint_choice::~joint_choice()
{
    delete ui;
}

void joint_choice::on_joints_list_itemClicked(QListWidgetItem *item)
{
    tracked_joint()=item->text();
}
