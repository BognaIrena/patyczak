#ifndef KINECT_DATA_H
#define KINECT_DATA_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QtSql>
#include <QDebug>
#include <QString>
#include <QDateTime>


/*!
 * \brief Połączenie do bazy danych.
 * \return 
 */
QSqlDatabase &kinect_data();
QDate &dzien(); /**< Zmienna z wzorca Singletona, przechowuje datę badania. */
QTime &czas (); /**< Zmienna z wzorca Singletona, przechowuje wybrany moment badania. */
int &badanieID(); /**< Zmienna z wzorca Singletona, przechowuje identyfikator badania. */

/*!
 * \brief Tryb działania programu.
 *
 * Zmienna pomocnicza określająca tryb wywołania metody PaintEvent.
 * 1 - tryb animacji całego szkieletu;
 * 0 - tryb śledzenia pojedynczego stawu.
 * \return
 */
int &tryb(); /**< Rodzaj flagi określającej tryb działania programu. */
QString &pacjent(); /**< Nazwisko wybranego pacjenta. */
QString &tracked_joint(); /**< Wybrany staw. */
int &point(); /**< Aktualny punkt animacji. */


QString WyswietlDate(const QDate &Date); /**< Funkcja przetwarzająca datę na zapis RRRR-MM-DD */

#endif // KINECT_DATA_H
