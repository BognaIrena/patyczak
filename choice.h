#ifndef CHOICE_H
#define CHOICE_H

#include <QDialog>
#include "ui_choice.h"
#include "kinect_data.h"
#include <QTableWidget>
#include <QTableWidgetItem>
#include <string>
#include <iostream>
#include <QDate>
namespace Ui
{
class Choice;
}

/*!
 * \brief Klasa wyboru "choice"
 *
 * Klasa powiązana z osobnym oknem dialogowym. Służy do wyboru trybu pracy (animacja szkieletu czy śledzenie stawu).
 */
class Choice : public QDialog
{
    Q_OBJECT

public:
    explicit Choice(QWidget *parent = 0);
    ~Choice();

private slots:
    /*!
     * \brief Wybór daty badania.
     *
     * Pozwala użytkownikowi wybrać interesującą go datę, zapisuje ją do globalnego użytku,
     * a następnie wysyła do bazy zapytanie o dane z tego dnia. Informacja o dostępnych danych jest
     * wysyłana do listWidget availableData, skąd użytkownik może wybrać konkretną sesję.
     *
     * \param date
     */
    void on_calendarWidget_clicked(const QDate &date);

    /*!
     * \brief Wybór sesji.
     *
     * Określa wybór użytkownika spośród sesji wykonanych jednego dnia. Identyfikator sesji jest zapisywany
     * do dalszego użytku, a dane pacjenta wyświetlane są w głównym oknie nad animacją.
     *
     * \param item
     */
    void on_availableData_cellClicked(int row, int column);

    void on_buttonBox_accepted(); /**< Zapisanie wyboru użytkownika i wprowadzenie go w życie. */

    void on_buttonBox_rejected(); /**< Odrzucenie dotychczasowych zmian; wyczyszczenie ewentualnie odczytanych danych pacjenta. */

private:
    Ui::Choice *ui;
};

#endif // CHOICE_H
