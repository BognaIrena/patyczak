#ifndef PATYCZAK_H
#define PATYCZAK_H

#include <QMainWindow>
#include <QWidget>
#include <QtSql>
#include "kinect_data.h"
#include "choice.h"
#include "ludzik.h"
#include <QTimer>


namespace Ui 
{

class Patyczak;
}
/*!
 * \brief Główne okno aplikacji "Patyczak".
 */
class Patyczak : public QMainWindow
{
    Q_OBJECT

public:
    explicit Patyczak(QWidget *parent = 0);
    ~Patyczak();

signals:

private slots:

    void on_data_choice_clicked(); /**< Wywołanie okna wyboru danych dla śledzenia całego szkieletu. */

    void on_joint_choice_clicked(); /**< Wywołanie okna wyboru danych dla śledzenia pojedynczego stawu. */

    void on_TimeSlider_sliderMoved(int position); /**< Zmiana momentu odtwarzania animacji. */

    void SetSlider();    /**< Slot odpowiedzialny za przesuwanie slidera w trakcie odtwarzania animacji.*/

private:
    Ui::Patyczak *ui;
};

#endif // PATYCZAK_H
