#include "choice.h"
#include "ui_choice.h"

Choice::Choice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Choice)
{
    ui->setupUi(this);
    setWindowTitle(tr("Wybór danych"));
}

Choice::~Choice()
{
    delete ui;
}

/*!
 * \brief Choice::on_calendarWidget_clicked
 * \param date
 *
 * Funkcja odpowiedzialna za sprawdzenie i wypisanie dostępnych danych z wybranego dnia.
 */
void Choice::on_calendarWidget_clicked(const QDate &date)
{
    dzien()=date;
/*!
 * Odczyt danych z bazy.
 */
    QSqlQuery odczyt;
    QString zpt("SELECT `badanie`.`ID`, `Person`.`ID`, `Name`, `Surname`"
                "from (`badanie` join `person` on (`badanie`.`Person_ID` = `person`.`ID`))"
                "where `badanie`.`Date` = '%1' order by `badanie`.`ID`");

    odczyt.exec(zpt.arg(WyswietlDate(date)));
//    qDebug() << zpt.arg(WyswietlDate(date)) << endl;
/*!
 * Zapisanie odebranych danych w widgecie avaiableData.
 */
    ui->availableData->clear();
    int k=0;
    ui->availableData->setColumnCount(4);
    ui->availableData->setHorizontalHeaderItem(0,new QTableWidgetItem("Nr"));
    ui->availableData->setHorizontalHeaderItem(1,new QTableWidgetItem("ID"));
    ui->availableData->setHorizontalHeaderItem(2,new QTableWidgetItem("Imię"));
    ui->availableData->setHorizontalHeaderItem(3,new QTableWidgetItem("Nazwisko"));

    while(odczyt.next())
    {
        ui->availableData->setRowCount(k+1);
        ui->availableData->setItem(k,0,new QTableWidgetItem(odczyt.value(0).toString()));
 //       qDebug() << "badanie ID: " << odczyt.value(0).toInt();

        for (int j=1; j<=3; j++)
        {
            ui->availableData->setItem(k,j,new QTableWidgetItem(odczyt.value(j).toString()));
  //          qDebug() << odczyt.value(j).toString();
        }
        ui->availableData->showRow(k);
        ++k;

    }
    ui->availableData->resizeColumnsToContents();
    ui->availableData->adjustSize();
    ui->availableData->show();
}

void Choice::on_buttonBox_accepted()
{
    this->close();
}

void Choice::on_availableData_cellClicked(int row, int column)
{
    badanieID()=ui->availableData->item(row, 0)->text().toInt();

//    qDebug() << "badanie_ID: " << badanieID();
    pacjent()=(ui->availableData->item(row, 2)->text()) +" "+(ui->availableData->item(row, 3)->text());
}

void Choice::on_buttonBox_rejected()
{
    badanieID()=-1;   /**< W razie odrzucenia wybieramy nieistniejące badanie, aby uniknąć "krzaków".*/
    pacjent()="null"; /**< W razie odrzucenia określamy brak pacjenta.*/
    this->close();
}
