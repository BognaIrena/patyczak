#include "patyczak.h"
#include "ui_patyczak.h"

Patyczak::Patyczak(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Patyczak)
{
    setWindowTitle(tr("Patyczak"));
    resize(400, 400);
    ui->setupUi(this);

    ui->TimeSlider->setTracking(0);
    ui->TimeSlider->setSingleStep(1);

  //  connect(ui->Start, SIGNAL(clicked()), ui->ludek, SLOT(Start()));
 //   connect(ui->Stop, SIGNAL(clicked()), ui->ludek, SLOT(Stop()));
}

Patyczak::~Patyczak()
{
    delete ui;
}


// odpytywanie po czasie (wybór danych z odpowiedniego zakresu czasu)
// dynamika ruchu poszczególnych przegubów (wykresy śladu, trajektoria)



void Patyczak::on_data_choice_clicked()
{
    Choice okienko;
    okienko.exec();
    ui->podpis->setText("Pacjent: "+pacjent()+ ". Tryb śledzenia postawy.");

    QSqlQuery odczyt;
       QString zpt_min ("SELECT `ID` FROM `position` WHERE `Badanie_ID`=%1 ORDER BY `ID` LIMIT 1");
       odczyt.exec(zpt_min.arg(badanieID()));

       int min, max;
       if (odczyt.next())
       {
           min=odczyt.value(0).toInt();
       }

       QString zpt_max ("SELECT `ID` FROM `position` WHERE `Badanie_ID`=%1 ORDER BY `ID` DESC LIMIT 1");
       odczyt.exec(zpt_max.arg(badanieID()));

       if (odczyt.next())
       {
           max=odczyt.value(0).toInt();
       }

    ui->TimeSlider->setRange(min, max);
 //   qDebug() << "min = " << min << " max " << max;
//    ui->ludek->update();
    ui->ludek->tryb=1;
    ui->ludek->repaint();

}

void Patyczak::on_joint_choice_clicked()
{
    Choice okienko;
    okienko.exec();
    ui->podpis->setText("Pacjent: "+ pacjent()+ ". Tryb śledzenia stawu: " + tracked_joint());

    QSqlQuery odczyt;
       QString zpt_min ("SELECT `ID` FROM `position` WHERE `Badanie_ID`=%1 ORDER BY `ID` LIMIT 1");
       odczyt.exec(zpt_min.arg(badanieID()));
  //      qDebug() << zpt_min.arg(badanieID());
       int min, max;
       if (odczyt.next())
       {
           min=odczyt.value(0).toInt();
       }

       QString zpt_max ("SELECT `ID` FROM `position` WHERE `Badanie_ID`=%1 ORDER BY `ID` DESC LIMIT 1");
       odczyt.exec(zpt_max.arg(badanieID()));
        qDebug() << zpt_max.arg(badanieID());
       if (odczyt.next())
       {
           max=odczyt.value(0).toInt();
       }

    ui->TimeSlider->setRange(min, max);
    qDebug() << "Slider min = " << min << " max " << max;

    joint_choice Lista;
    Lista.exec();
    ui->podpis->setText("Pacjent: "+ pacjent()+ ". Tryb śledzenia stawu: " + tracked_joint());
//    ui->ludek->update();
    ui->ludek->tryb=0;
    ui->ludek->repaint();

}

void Patyczak::on_TimeSlider_sliderMoved(int position)
{
    czas().addMSecs(czas().msec()+position);
    point()=position;
    //    ui->ludek->repaint();
}

void Patyczak::SetSlider()
{
    ui->TimeSlider->setValue(point());
}

