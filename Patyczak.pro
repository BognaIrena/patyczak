#-------------------------------------------------
#
# Project created by QtCreator 2014-04-10T20:39:33
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Patyczak
TEMPLATE = app

#LIBS += -LC:/Qt/Qt5.3.0/5.3/mingw482_32/bin/Qt5Sql.dll
#LIBS += -LC:/Qt/Qt5.3.0/5.3/mingw482_32/bin/Qt5Sqld.dll
#LIBS += -LC:/Qt/Qt5.3.0/5.3/mingw482_32/bin/libmysql.dll

#LIBS += -LC:/Qt/Qt5.3.0/5.3/mingw482_32/plugins/sqldrivers/qsqlmysql.dll
#LIBS += -LD:/build-Patyczak-Desktop_Qt_5_2_1_MinGW_32bit-Debug/debug/qsqlmysql.dll

SOURCES += main.cpp\
        patyczak.cpp \
    ludzik.cpp \
    kinect_data.cpp \
    choice.cpp \
    staw.cpp \
    joint_choice.cpp

HEADERS  += patyczak.h \
    ludzik.h \
    kinect_data.h \
    choice.h \
    staw.h \
    joint_choice.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/build_config.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/config.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/connection.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/datatype.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/driver.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/exception.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/metadata.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/parameter_metadata.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/prepared_statement.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/resultset.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/resultset_metadata.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/sqlstring.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/statement.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/warning.h \
#    ../MySQL/Connector C++ 1.1.3/include/mysql_connection.h \
#    ../MySQL/Connector C++ 1.1.3/include/mysql_driver.h \
#    ../MySQL/Connector C++ 1.1.3/include/mysql_error.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/build_config.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/config.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/connection.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/datatype.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/driver.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/exception.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/metadata.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/parameter_metadata.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/prepared_statement.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/resultset.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/resultset_metadata.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/sqlstring.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/statement.h \
#    ../MySQL/Connector C++ 1.1.3/include/cppconn/warning.h \
#    ../MySQL/Connector C++ 1.1.3/include/mysql_connection.h \
#    ../MySQL/Connector C++ 1.1.3/include/mysql_driver.h \
#    ../MySQL/Connector C++ 1.1.3/include/mysql_error.h \
#    Connector C++ 1.1.3/include/cppconn/build_config.h \
#    Connector C++ 1.1.3/include/cppconn/config.h \
#    Connector C++ 1.1.3/include/cppconn/connection.h \
#    Connector C++ 1.1.3/include/cppconn/datatype.h \
#    Connector C++ 1.1.3/include/cppconn/driver.h \
#    Connector C++ 1.1.3/include/cppconn/exception.h \
#    Connector C++ 1.1.3/include/cppconn/metadata.h \
#    Connector C++ 1.1.3/include/cppconn/parameter_metadata.h \
#    Connector C++ 1.1.3/include/cppconn/prepared_statement.h \
#    Connector C++ 1.1.3/include/cppconn/resultset.h \
#    Connector C++ 1.1.3/include/cppconn/resultset_metadata.h \
#    Connector C++ 1.1.3/include/cppconn/sqlstring.h \
#    Connector C++ 1.1.3/include/cppconn/statement.h \
#    Connector C++ 1.1.3/include/cppconn/warning.h \
#    Connector C++ 1.1.3/include/mysql_connection.h \
#    Connector C++ 1.1.3/include/mysql_driver.h \
#    Connector C++ 1.1.3/include/mysql_error.h \
#    Connector C++ 1.1.3/include/cppconn/build_config.h \
#    Connector C++ 1.1.3/include/cppconn/config.h \
#    Connector C++ 1.1.3/include/cppconn/connection.h \
#    Connector C++ 1.1.3/include/cppconn/datatype.h \
#    Connector C++ 1.1.3/include/cppconn/driver.h \
#    Connector C++ 1.1.3/include/cppconn/exception.h \
#    Connector C++ 1.1.3/include/cppconn/metadata.h \
#    Connector C++ 1.1.3/include/cppconn/parameter_metadata.h \
#    Connector C++ 1.1.3/include/cppconn/prepared_statement.h \
#    Connector C++ 1.1.3/include/cppconn/resultset.h \
#    Connector C++ 1.1.3/include/cppconn/resultset_metadata.h \
#    Connector C++ 1.1.3/include/cppconn/sqlstring.h \
#    Connector C++ 1.1.3/include/cppconn/statement.h \
#    Connector C++ 1.1.3/include/cppconn/warning.h \
#    Connector C++ 1.1.3/include/mysql_connection.h \
#    Connector C++ 1.1.3/include/mysql_driver.h \
#    Connector C++ 1.1.3/include/mysql_error.h \
#    mainwindow.h \
#    Connector C++ 1.1.3/include/mysql_connection.h \
#    Connector C++ 1.1.3/include/mysql_driver.h \
#    Connector C++ 1.1.3/include/mysql_error.h \
#    Connector C++ 1.1.3/include/mysql_connection.h \
#    Connector C++ 1.1.3/include/mysql_driver.h \
#    Connector C++ 1.1.3/include/mysql_error.h \
#    Connector C++ 1.1.3/include/cppconn/build_config.h \
#    Connector C++ 1.1.3/include/cppconn/config.h \
#    Connector C++ 1.1.3/include/cppconn/connection.h \
#    Connector C++ 1.1.3/include/cppconn/datatype.h \
#    Connector C++ 1.1.3/include/cppconn/driver.h \
#    Connector C++ 1.1.3/include/cppconn/exception.h \
#    Connector C++ 1.1.3/include/cppconn/metadata.h \
#    Connector C++ 1.1.3/include/cppconn/parameter_metadata.h \
#    Connector C++ 1.1.3/include/cppconn/prepared_statement.h \
#    Connector C++ 1.1.3/include/cppconn/resultset.h \
#    Connector C++ 1.1.3/include/cppconn/resultset_metadata.h \
#    Connector C++ 1.1.3/include/cppconn/sqlstring.h \
#    Connector C++ 1.1.3/include/cppconn/statement.h \
#    Connector C++ 1.1.3/include/cppconn/warning.h \
#    Connector C++ 1.1.3/include/mysql_connection.h \
#    Connector C++ 1.1.3/include/mysql_driver.h \
#    Connector C++ 1.1.3/include/mysql_error.h \
#    Connector C++ 1.1.3/include/mysql_connection.h \
#    Connector C++ 1.1.3/include/mysql_driver.h \
#    Connector C++ 1.1.3/include/mysql_error.h

FORMS    += patyczak.ui \
    choice.ui \
    joint_choice.ui

#LIBS+= D:/wamp/bin/mysql/mysql5.6.12/lib/libmysql.dll


#LIBS += "-LD:/wamp/bin/mysql/mysql5.6.12/lib/" -lmysql
#INCLUDEPATH+="D:/wamp/bin/mysql/mysql5.6.12/include"
#INCLUDEPATH+="D:/MySQL/Connector ODBC 5.3.3/include"
#INCLUDEPATH+="D:/MySQL/Connector C++ 1.1.3/include"


INCLUDEPATH += $$PWD/
DEPENDPATH += $$PWD/

#win32: LIBS += -L$$PWD/ -lbcbmysql

INCLUDEPATH += $$PWD/
DEPENDPATH += $$PWD/
