#ifndef STAW_H
#define STAW_H

#include <QString>
#include <QPoint>
#include <QDateTime>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include "kinect_data.h"

/*!
 * \brief Struktura staw przechowuje informację o położeniu danego stawu w danym momencie.
 */
struct staw
{
    QString nazwa;      /**< Nazwa stawu, wykorzystywana przy tworzeniu zapytania do bazy. */
    QPoint position;    /**< QPoint wykorzystywany do rysowania postaci. */
    bool powodzenie;    /**< Flaga mówiąca o powodzeniu odczytu pozycji z bazy. */

    staw(QString name); /**< Konstruktor parametryczny, służy do tworzenia stawu konkretnego rodzaju. */
    staw(); /**< Standardowy konstruktor bezparametryczny, ustawia jedynie flagę powodzenia operacji. */

    /*!
     * \brief odczyt
     *
     * Metoda struktury "staw" odpowiedzialna za wysłanie zapytania do bazy danych oraz
     * zapisanie odpowiedzi na temat kolejnego położenia stawu.
     * \param czas
     * \return
     */
    staw & odczyt (QTime czas); /**< Czas aktualnego odczytu; w tej wersji programu nie używany.*/
    staw & odczyt (int point); /**< Indeks odczytywanego szkieletu.*/
};

#endif // STAW_H
